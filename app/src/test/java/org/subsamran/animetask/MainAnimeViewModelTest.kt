package org.subsamran.animetask

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.subsamran.animetask.api.ApiHelper
import org.subsamran.animetask.dao.AnimeCollection
import org.subsamran.animetask.main.MainAnimeViewModel
import org.subsamran.animetask.utils.Resource
import org.subsamran.animetask.utils.TestCoroutineRule

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainAnimeViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper : ApiHelper

    @Mock
    private lateinit var apiAnimeObserver: Observer<Resource<AnimeCollection>>

    @Before
    fun setUp() {
        // do something if required
    }

    @Test
    fun test_top_anime_response_200_when_fetch_should_return_success() {
        testCoroutineRule.runBlockingTest {
            var dataCollectionSuccess = AnimeCollection()
            Mockito.doReturn(dataCollectionSuccess)
                .`when`(apiHelper)
                .getDataAnime()

            val viewModel = MainAnimeViewModel(apiHelper)
            viewModel.loadAnime().observeForever(apiAnimeObserver)
            Mockito.verify(apiHelper).getDataAnime()
            Mockito.verify(apiAnimeObserver).onChanged(Resource.success(dataCollectionSuccess))
            viewModel.loadAnime().removeObserver(apiAnimeObserver)
        }
    }

    @Test
    fun test_top_anime_response_error_when_fetch_should_return_error() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Error load data"
            Mockito.doThrow(RuntimeException(errorMessage))
                .`when`(apiHelper)
                .getDataAnime()
            val viewModel = MainAnimeViewModel(apiHelper)
            viewModel.loadAnime().observeForever(apiAnimeObserver)
            Mockito.verify(apiHelper).getDataAnime()
            Mockito.verify(apiAnimeObserver).onChanged(
                Resource.error(
                    RuntimeException(errorMessage).toString(),
                    null
                )
            )
            viewModel.loadAnime().removeObserver(apiAnimeObserver)
        }
    }

}