package org.subsamran.animetask.main

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import org.subsamran.animetask.api.ApiHelper
import org.subsamran.animetask.dao.Anime
import org.subsamran.animetask.dao.AnimeCollection
import org.subsamran.animetask.utils.Resource

class ViewModelFactory(private val apiHelper: ApiHelper) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainAnimeViewModel(apiHelper) as T
    }
}

class MainAnimeViewModel(private val apiHelper: ApiHelper) : ViewModel() {

    private val dataAnimeCollection = MutableLiveData<Resource<AnimeCollection>>()
    val dataAnime = MutableLiveData<List<Anime?>?>()

    init {
        fetchDataAnime()
    }

    private fun fetchDataAnime() {
        viewModelScope.launch {
            dataAnimeCollection.postValue(Resource.loading(null))
            try {
                val dataFromApi = apiHelper.getDataAnime()
                dataAnimeCollection.postValue(Resource.success(dataFromApi))
            } catch (e: Exception) {
                dataAnimeCollection.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun loadAnime(): LiveData<Resource<AnimeCollection>> {
        return dataAnimeCollection
    }

    fun setDataAnime(data: List<Anime?>?) {
        dataAnime.value = data
    }
}