package org.subsamran.animetask.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.anime_item.view.*
import org.subsamran.animetask.MyApp
import org.subsamran.animetask.R
import org.subsamran.animetask.dao.Anime

class MainAnimeAdapter(private val listener: MainAnimeAdapterListener) : RecyclerView.Adapter<MainAnimeAdapter.ViewHolder>() {

    private var data: List<Anime?>? = null

    init {
        data = arrayListOf()
    }

    fun setData(data: List<Anime?>?) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.anime_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data!![position])
        holder.itemView.setOnClickListener {
            listener.onAnimeClick(data!![position])
        }
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(anime: Anime?) {
            itemView.name.text = anime!!.title
            Glide.with(MyApp.getContext()!!)
                .load(anime.images!!.jpg!!.imageUrl)
                .placeholder(R.color.black)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade(500))
                .into(itemView.img)
        }
    }

    interface MainAnimeAdapterListener {
        fun onAnimeClick(anime: Anime?)
    }
}