package org.subsamran.animetask.main

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import org.subsamran.animetask.R
import org.subsamran.animetask.api.ApiHelperImpl
import org.subsamran.animetask.api.RetrofitBuilder
import org.subsamran.animetask.dao.Anime
import org.subsamran.animetask.databinding.MainAnimeActivityBinding
import org.subsamran.animetask.detail.DetailAnimeActivity
import org.subsamran.animetask.utils.Status

class MainAnimeActivity : AppCompatActivity() {

    private lateinit var binding: MainAnimeActivityBinding
    private lateinit var viewModel: MainAnimeViewModel
    private lateinit var animeAdapter: MainAnimeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_anime_activity)
        binding.lifecycleOwner = this
        supportActionBar!!.title = getString(R.string.title_name)
        viewModel = ViewModelProvider(this, ViewModelFactory(ApiHelperImpl(RetrofitBuilder.apiService)))[MainAnimeViewModel::class.java]
        binding.viewModel = viewModel

        animeAdapter = MainAnimeAdapter(object :MainAnimeAdapter.MainAnimeAdapterListener{
            override fun onAnimeClick(anime: Anime?) {
                DetailAnimeActivity.newIntent(this@MainAnimeActivity, anime = anime!!)
            }
        })

        binding.mainAnime.apply {
            layoutManager =
                LinearLayoutManager(this@MainAnimeActivity, LinearLayoutManager.VERTICAL, false)
            adapter = animeAdapter
        }
        setupObserver()
    }

    private fun setupObserver() {

        viewModel.dataAnime.observe(this) {
            animeAdapter.setData(it)
        }

        viewModel.loadAnime().observe(this) {
            when (it.status) {
                Status.LOADING -> {

                }
                Status.SUCCESS -> {
                    viewModel.setDataAnime(it.data?.data!!)
                }
                Status.ERROR -> {
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}