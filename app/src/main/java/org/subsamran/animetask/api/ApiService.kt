package org.subsamran.animetask.api

import org.subsamran.animetask.dao.AnimeCollection
import retrofit2.http.GET

interface ApiService {

    @GET("top/anime")
    suspend fun getDataFundsCollection(): AnimeCollection
}