package org.subsamran.animetask.api

import org.subsamran.animetask.dao.AnimeCollection

class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {
    override suspend fun getDataAnime(): AnimeCollection {
        return apiService.getDataFundsCollection()
    }
}