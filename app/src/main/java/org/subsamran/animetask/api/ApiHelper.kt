package org.subsamran.animetask.api

import org.subsamran.animetask.dao.AnimeCollection

interface ApiHelper {
    suspend fun getDataAnime() : AnimeCollection
}