package org.subsamran.animetask.dao

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AnimeCollection {
    @SerializedName("pagination")
    @Expose
     val pagination: Pagination? = null

    @SerializedName("data")
    @Expose
     val data: List<Anime>? = null

    @SerializedName("links")
    @Expose
     val links: Links? = null

    @SerializedName("meta")
    @Expose
     val meta: Meta? = null
}