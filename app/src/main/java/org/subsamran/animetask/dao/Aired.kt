package org.subsamran.animetask.dao

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Aired {
    @SerializedName("from")
    @Expose
    private var from: String? = null

    @SerializedName("to")
    @Expose
    private var to: String? = null

    @SerializedName("prop")
    var prop: Prop? = null

    @SerializedName("string")
    @Expose
     var string: String? = null
}