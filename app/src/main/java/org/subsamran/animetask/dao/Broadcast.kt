package org.subsamran.animetask.dao

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Broadcast {
    @SerializedName("day")
    @Expose
     var day: String? = null

    @SerializedName("time")
    @Expose
     var time: String? = null

    @SerializedName("timezone")
    @Expose
     var timezone: String? = null

    @SerializedName("string")
    @Expose
     var string: String? = null
}