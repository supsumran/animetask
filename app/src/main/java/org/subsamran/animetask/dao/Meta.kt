package org.subsamran.animetask.dao

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Meta {
    @SerializedName("current_page")
    @Expose
     val currentPage: Int? = null

    @SerializedName("from")
    @Expose
     val from: Int? = null

    @SerializedName("last_page")
    @Expose
     val lastPage: Int? = null

    @SerializedName("links")
    @Expose
     val links: List<Link>? = null

    @SerializedName("path")
    @Expose
     val path: String? = null

    @SerializedName("per_page")
    @Expose
     val perPage: Int? = null

    @SerializedName("to")
    @Expose
     val to: Int? = null

    @SerializedName("total")
    @Expose
     val total: Int? = null
}