package org.subsamran.animetask.dao

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Links {
    @SerializedName("first")
    @Expose
     val first: String? = null

    @SerializedName("last")
    @Expose
     val last: String? = null

    @SerializedName("prev")
    @Expose
     val prev: Any? = null

    @SerializedName("next")
    @Expose
     val next: String? = null
}