package org.subsamran.animetask.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}