package org.subsamran.animetask.detail

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.anime_item.view.*
import org.subsamran.animetask.MyApp
import org.subsamran.animetask.R
import org.subsamran.animetask.dao.Anime
import org.subsamran.animetask.databinding.DetailAnimeActivityBinding

class DetailAnimeActivity : AppCompatActivity() {

    companion object {
        lateinit var anime: Anime
        fun newIntent(context: Context,anime: Anime) {
            this.anime = anime
            var intent = Intent(context, DetailAnimeActivity::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var binding: DetailAnimeActivityBinding
    lateinit var viewmodel : DetailAnimeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.detail_anime_activity)
        binding.lifecycleOwner = this
        viewmodel = ViewModelProvider(this, ViewModelFactory(anime))[DetailAnimeViewModel::class.java]
        binding.viewModel = viewmodel
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = anime.title

        viewmodel.largeImg.observe(this) {
            Glide.with(MyApp.getContext()!!)
                .load(anime.images!!.jpg!!.largeImageUrl)
                .placeholder(R.color.black)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade(500))
                .into(binding.largeImg)
        }

        binding.setTrailerClickListener {
            openYoutubeLink(anime.trailer!!.url.toString())
        }
    }

    private fun openYoutubeLink(url: String) {
        val intentBrowser = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        try {
            this.startActivity(intentBrowser)
        } catch (ex: ActivityNotFoundException) {
            this.startActivity(intentBrowser)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}