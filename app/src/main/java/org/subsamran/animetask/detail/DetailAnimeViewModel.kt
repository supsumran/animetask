package org.subsamran.animetask.detail

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.subsamran.animetask.dao.Anime

class ViewModelFactory(private val anime: Anime) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailAnimeViewModel(anime) as T
    }
}

class DetailAnimeViewModel(private val anime: Anime): ViewModel() {
    val largeImg = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val status = MutableLiveData<String>()
    val synopsis = MutableLiveData<String>()
    val rating = MutableLiveData<String>()
    val year = MutableLiveData<String>()
    val aired = MutableLiveData<String>()

    init {
        largeImg.value = anime.images!!.jpg!!.largeImageUrl
        title.value = anime.title
        status.value = anime.status
        synopsis.value = anime.synopsis
        rating.value = anime.rating
        year.value = anime.year.toString()
        aired.value = anime.aired!!.string
    }
}